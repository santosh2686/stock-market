const { merge } = require('webpack-merge')
const commonConfig = require('./webpack.common')

const devConfig = {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    port: process.env.PORT || 5050,
    host: '0.0.0.0',
    historyApiFallback: true,
    disableHostCheck: true,
    overlay: true,
    compress: true,
    proxy: [{
      path: '/v7/finance/quote',
      target: 'https://query1.finance.yahoo.com',
      secure: false,
      changeOrigin: true,
    }],
  },
}

module.exports = merge(commonConfig, devConfig)
