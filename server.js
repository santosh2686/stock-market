const path = require('path')
const express = require('express')
const compression = require('compression')
const proxyMiddleware = require('http-proxy-middleware')

const app = express()
const port = process.env.PORT || 6060
const apiEndPoint = 'https://query1.finance.yahoo.com'

app.use(compression())
app.use(express.static(`${__dirname}/build`))

app.use('/v7/finance/quote', proxyMiddleware({
  target: apiEndPoint,
  changeOrigin: true,
  secure: false,
  pathRewrite: {
    '^/api/': '/',
  },
  onProxyReq: (proxyReq, req) => {
    console.log(req.method, req.path, '->', apiEndPoint + proxyReq.path)
  },
}))

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'))
})

app.listen(port, () => {
  console.log(`Application Running on port: ${port}`)
})
