import React, { memo } from 'react'
import { string } from 'prop-types'
import { classNames } from 'design-pattern-library/util'

import './style.scss'

const blk = 'company-name'

const CompanyName = ({ symbol, name }) => (
  <div className={blk}>
    {symbol}
    <span className={classNames({ blk, elt: 'full-name' })}>{name}</span>
  </div>
)

CompanyName.propTypes = {
  symbol: string,
  name: string,
}

CompanyName.defaultProps = {
  symbol: '',
  name: '',
}

export default memo(CompanyName)
