import React, { memo } from 'react'
import { bool, number } from 'prop-types'
import { classNames } from 'design-pattern-library/util'

import { numberToCurrency } from '@util'

import './style.scss'

const blk = 'price'

const Price = ({
  value, withHighLow, withPercentage,
}) => {
  const eltClass = classNames({
    blk,
    mods: {
      high: withHighLow && value > 0,
      low: withHighLow && value < 0,
    },
  })

  return (
    <div className={eltClass}>
      {numberToCurrency.format(value, 2)}
      {withPercentage && '%'}
    </div>
  )
}

Price.propTypes = {
  value: number.isRequired,
  withHighLow: bool,
  withPercentage: bool,
}

Price.defaultProps = {
  withHighLow: false,
  withPercentage: false,
}

export default memo(Price)
