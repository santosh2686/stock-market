/* eslint-disable */
import React, { PureComponent } from 'react'
import axios from 'axios'
import { classNames } from 'design-pattern-library/util'

import { Table, Spinner } from '@common'

import { symbolList } from '@config'

import CompanyName from './components/companyName'
import Price from './components/price'

import './style.scss'

const blk = 'stock-list'

const priceClass = 'text-right'

class StockList extends PureComponent {
  state = {
    isLoading: false,
    data: [],
  }

  fetchList = () => {
    const params = {
      symbols: symbolList.join(','),
    }
    axios.get('/v7/finance/quote', { params }).then((res) => {
      const { data = {} } = res
      const { quoteResponse } = data
      this.setState({
        data: quoteResponse.result,
        isLoading: false,
      })
    })
  }

  componentDidMount() {
    this.setState({ isLoading: true })
    this.fetchList()
    setInterval(this.fetchList, 2000)
  }

  render() {
    const { isLoading, data } = this.state
    const columns = [
      {
        title: 'Company',
        custom: ({ longName, symbol }) => (
          <CompanyName symbol={symbol} name={longName} />
        ),
      },
      {
        title: 'Price',
        className: priceClass,
        custom: ({ regularMarketPrice }) => (
          <Price value={regularMarketPrice} />
        ),
      },
      {
        title: 'Change value',
        className: priceClass,
        custom: ({ regularMarketChange }) => (
          <Price value={regularMarketChange} withHighLow />
        ),
      },
      {
        title: 'Change percentage',
        className: priceClass,
        custom: ({ regularMarketChangePercent }) => (
          <Price value={regularMarketChangePercent} withPercentage withHighLow />
        ),
      },
      {
        title: 'Volume',
        className: priceClass,
        map: 'regularMarketVolume',
      },
    ]
    if (isLoading) {
      return (
        <Spinner className={classNames({ blk, elt: 'spinner' })} />
      )
    }

    return (
      <div className={blk}>
        <Table columns={columns} data={data} className={classNames({ blk, elt: 'table' })} />
      </div>
    )
  }
}

export default StockList
