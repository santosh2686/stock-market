const symbolList = ['TSLA', 'AAPL', 'WMT', 'GE', 'GS', 'GM', 'TGT', 'ABNB', 'DASH',
  'MRNA', 'SQ', 'ORCL', 'MDB', 'SHOP', 'BA', 'TWTR', 'NFLX', 'FB', 'MSFT', 'DIS',
  'GPRO', 'SBUX', 'F', 'BAC', 'AMZN',
]

export {
  symbolList,
}
