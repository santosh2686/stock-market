import React from 'react'
import { classNames } from 'design-pattern-library/util'

import { Container } from '@common'

import './style.scss'

const blk = 'header'

const Header = () => (
  <div className={blk}>
    <Container>
      <h1 className={classNames({ blk, elt: 'title' })}>Stock</h1>
    </Container>
  </div>
)

export default Header
