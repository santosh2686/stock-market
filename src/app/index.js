import React from 'react'

import { Container } from '@common'

import Header from '../header'
import StockList from '../stock-list'

import './style.scss'

const App = () => (
  <>
    <Header />
    <Container>
      <StockList />
    </Container>
  </>
)

export default App
