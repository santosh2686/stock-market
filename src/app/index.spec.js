import React from 'react'
import { shallow } from 'enzyme'

import App from './index'

describe('App Component', () => {
  let wrapper
  beforeAll(() => {
    const component = <App />
    wrapper = shallow(component)
  })
  it('should render', () => {
    expect(wrapper).toBeDefined()
  })
})
